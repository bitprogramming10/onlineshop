<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;

class NormalController extends Controller
{
    public function home(){
        return view('frontend.index');
    }
    public function contact(){
        return view('frontend.contact');
    }
    public function about(){
        return view('frontend.about');
    }
    public function shop(){
        $products       =       Product::paginate(8);
        return view('frontend.shop')->with('products',$products);
    }
    public function productDetails($id){
        $product        =       Product::find($id);
        return view('frontend.single-product')->with('product',$product);
    }
}
