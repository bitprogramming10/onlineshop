@extends('layout.backend.app')

@section('content')

    <style>
        h2{
            text-align: center;
        }
        .form{
            margin: 1cm;
        }
        .custom-margin{
            margin-top: 1cm;
        }
        .custom-center{
            text-align: center;
        }
    </style>

    <div class="form">
        <h2> Enter Details Below:</h2>
        <form action="{{ url('admin/product/store') }}" method="post">
            @csrf
            <div class="row custom-margin">
                <div class="col-md-4">Product Name:</div>
                <div class="col-md-8"><input type="text" name="name" class="form-control" placeholder="Nike Sport Shoes"></div>
            </div>
            <div class="row custom-margin">
                <div class="col-md-4">Product Price:</div>
                <div class="col-md-8"><input type="text" name="price" class="form-control" placeholder="3500"></div>
            </div>
            <div class="row custom-margin">
                <div class="col-md-4">Product Color:</div>
                <div class="col-md-8"><input type="text" name="color" class="form-control" placeholder="black"></div>
            </div>
            <div class="row custom-margin">
                <div class="col-md-4">Product Size:</div>
                <div class="col-md-8"><input type="text" name="size" class="form-control" placeholder="XL"></div>
            </div>
            <div class="row custom-margin">
                <div class="col-md-4">Product Description:</div>
                <div class="col-md-8"><textarea name="description" class="form-control" placeholder="this is brand new...."></textarea></div>
            </div>
            <div class="row custom-margin custom-center">
                <input type="submit" class="btn btn-primary" value="Add new item to Store">
            </div>
        </form>
    </div>

@endsection