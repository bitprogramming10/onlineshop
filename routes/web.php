<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('')->group(function() {
    Route::get('/'                      , 'NormalController@home');
    Route::get('/contact'               , 'NormalController@contact');
    Route::get('/about'                 , 'NormalController@about');
    Route::get('/shop'                  , 'NormalController@shop');
    Route::get('/single-product/{id}'   , 'NormalController@productDetails');
});
Route::prefix('admin')->group(function() {
    Route::get('/product/'                      , 'ProductController@index');
    Route::get('/product/create'                , 'ProductController@create');
    Route::post('/product/store'                , 'ProductController@store');
    Route::get('/product/edit/{id}'             , 'ProductController@edit');
    Route::post('/product/update/{id}'          , 'ProductController@update');
    Route::get('/product/delete/{id}'           , 'ProductController@destroy');
});
//Route::prefix('')->group(function() {
//    Route::get('/'                      , 'NormalController@home');
//    Route::get('/contact'               , 'NormalController@contact');
//    Route::get('/about'                 , 'NormalController@about');
//});